/*
TinyVM - B4A 64-Bit Virtual Machine
Copyright (C) 2017  Bruno Silva / Ninja Dynamics
Homepage: https://gitlab.com/brunowonder/TinyVM

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


//*******************************************************************
//* NLG CODE STARTS HERE                                            *
//*******************************************************************
#include "tinyvm.h"

//Initializes the Virtual Machine
void Initialize(void)
{
	//Memory allocation
	VM = (TinyVM*)SafeMalloc(sizeof(TinyVM));

	//Endianess Test
	volatile uint32_t i         = 0x01234567;
	VM->littleEndian            = (*((uint8_t*)(&i))) == 0x67;

	//Members
	VM->sizeOfInt64             = sizeof(int64_t);
	VM->sizeOfData64            = sizeof(data64);
	VM->sizeOfByte              = sizeof(byte);
	VM->sizeOfChar              = sizeof(char);
	VM->sizeOfInstruction       = sizeof(byte) + sizeof(int64_t) + sizeof(byte);
	VM->memoryAddress           = (uintptr_t)VM;
	VM->state                   = VMSTATE_STOPPED;
	VM->stack                   = NULL;
	VM->livecode                = NULL;
	VM->bytecode                = NULL;
	VM->sizeOfBytecode          =  0;
	VM->nInstructions           =  0;
	VM->ip                      =  0;
	VM->sp                      = -1;

	//Stack
	SetStackLength(256);

	//Output Message
	printf("TinyVM - Initialized.\n");
}

//Destroys the Virtual Machine
void Kill(void)
{
	free(VM->bytecode);
	free(VM->livecode);
	free(VM);

	//Output Message
	printf("TinyVM - Destroyed.\n");
}

//Compiles the given assembly code and loads the Virtual Machine with the resulting bytecode
void Compile(char* code)
{
	size_t a, i, j, k, lc, p;
	size_t lenCode = strlen(code);
	char* assemblyCode = NULL;
	dynamicTable *labels = NewTable();

	//Appends a new line character at the end of the code string, if necessary
	if (code[lenCode - 1] != '\n') {
		lenCode += 2;
		assemblyCode = (char*)SafeMalloc(VM->sizeOfChar * lenCode);
		for (i = 0; i < lenCode - 2; ++i) {
			assemblyCode[i] = code[i];
		}
		assemblyCode[lenCode - 2] = '\n';
		assemblyCode[lenCode - 1] = '\0';
	}
	else {
		lenCode += 1;
		assemblyCode = (char*)SafeMalloc(VM->sizeOfChar * (lenCode));
		for (i = 0; i < lenCode; ++i) {
			assemblyCode[i] = code[i];
		}
	}

	//Counts the instructions
	char label[16];
	byte isInstruction = 0;
	byte isComment = 0;
	byte isLabel = 0;
	for (i = 0; i < lenCode; ++i) {
		if (!isLabel && !isComment && assemblyCode[i] == ':') {
			MakeSureThat(LABEL_IS_NOT_AN_EMPTY_STRING);
			j = i; while (assemblyCode[j] != '\n') {
				if (j == 0) break; --j;
			} if (j > 0) ++j;
			MakeSureThat(LABEL_DOESNT_START_WITH_A_DIGIT);
			k = 0; while (assemblyCode[j] && assemblyCode[j] != ':' && k < 16)
			{
				label[k] = assemblyCode[j];
				UPPERCASE(label[k]);
				++j; ++k;
			}
			MakeSureThat(LABEL_DOESNT_EXCEED_MAXLENGTH);
			label[k] = '\0';
			AddNode(labels, label, VM->nInstructions);
			isLabel = 1;
			isInstruction = 0;
		}
		if (assemblyCode[i] == ';') {
			isComment = 1;
		}
		if (!isLabel && !isComment && assemblyCode[i] > 32 && assemblyCode[i] < 127) {
			isInstruction = 1;
		}
		if (assemblyCode[i] == '\n') {
			if (isInstruction) ++VM->nInstructions;
			isInstruction = 0;
			isComment = 0;
			isLabel = 0;
		}
	}

	//Allocates memory for the bytecode array
	VM->sizeOfBytecode = VM->sizeOfInstruction * (size_t)(VM->nInstructions + 1);
	if (VM->bytecode) free(VM->bytecode);
	VM->bytecode = (byte*)SafeMalloc(VM->sizeOfBytecode);

	//Starts the debug output
	printf("TinyVM - Instructions loaded:\n");
	printf("-----------------------------------------");

	data64 data;
	a = 0; i = 0; j = 0;
	char textOutput[80];
	textOutput[0] = '\0';
	if (VM->nInstructions > 0) {
		//Populates the bytecode array
		double value;
		char   tmp[80];
		size_t instIndex = 0;
		byte   component = 0, reg = 0, isRegister = 0, isJumpInstruction = 0;
		while (assemblyCode[a]) {
			if (assemblyCode[a] == ' ' || assemblyCode[a] == '\n') {
				//Fetches the last character
				tmp[j] = '\0'; lc = max(0, (int)(j - 1)); if (lc == '\r') lc = max(0, (int)(j - 2)); j = 0;
				if (*tmp) {
					switch (component) {
					case 1:
						printf("%s", textOutput); textOutput[0] = '\0';
						if (tmp[lc] == ':') {
							ADD_STRING_ARGS_TO(textOutput, "\n%8s", tmp);
							component = 4;
							break;
						}
						VM->bytecode[i] = 0;
						isJumpInstruction = 0;
						for (k = 0; k < NUMBER_OF_INSTRUCTIONS; k++) {
							if (!strcmp(instructionTable[k].key, tmp)) {
								ADD_STRING_ARGS_TO(textOutput, "\n%16s ", tmp);
								VM->bytecode[i++] = instructionTable[k].val;
								if (tmp[0] == 'J') isJumpInstruction = 1;
								break;
							}
						}
						++component;
						tmp[0] = '\0';
						break;

					case 2:
						value = -1;
						isRegister = 0;
						if (isJumpInstruction) {
							tableNode* node = labels->rootNode;
							while (node)
							{
								if (!strcmp(node->key, tmp)) {
									value = (double)node->val;
									break;
								}
								node = node->next;
							}
						}
						else if (tmp[0] == 'R' || tmp[0] == '%') {
							isRegister = 1;
						}
						if (!isJumpInstruction && isRegister && tmp[2] == ',') {
							MakeSureThat(THERE_ARE_NO_SPACES_BETWEEN_ARGS);
							data.i32[0] = tmp[1] - '0';
							if (tmp[3] == 'R') {
								data.i32[1] = tmp[4] - '0';
								ADD_STRING_ARGS_TO(textOutput, "% 8c%d,% 7c%d", tmp[0], data.i32[0], tmp[3], data.i32[1]);
							}
							else {
								value = StrToNum(tmp + 3);
								data.i32[1] = (int32_t)value;
								if (data.i32[1] != value) {
									data.f32[1] = (float)value;
								}
								ADD_STRING_ARGS_TO(textOutput, "% 8c%d,% 8d", tmp[0], data.i32[0], data.i32[1]);
							}
						}
						else {
							if (value < 0) {
								value = StrToNum(tmp + isRegister);
							}
							data.i64 = (int64_t)value;
							if (isRegister) {
								ADD_STRING_ARGS_TO(textOutput, "% 8c%lld", tmp[0], data.i64);
							}
							else if (data.i64 != value) {
								data.f64 = value;
								ADD_STRING_ARGS_TO(textOutput, "% 18f", data.f64);
							}
							else {
								ADD_STRING_ARGS_TO(textOutput, "% 18lld", data.i64);
							}
						}
						for (p = 0; p < VM->sizeOfInt64; ++p) {
							VM->bytecode[i++] = ((byte*)(&data.i64))[p];
						}
						ADD_STRING_TO(textOutput, " ");
						++component;
						tmp[0] = '\0';
						break;

					case 3:
						if (tmp[0] == 'R' || tmp[0] == '%') {
							reg = (byte)tmp[1] - '0';
						}
						else {
							reg = (byte)tmp[0] - '0';
						}
						if (reg > 0 && reg < 10) {
							VM->bytecode[i++] = reg;
						}
						else {
							VM->bytecode[i++] = 0;
						}
						ADD_STRING_ARGS_TO(textOutput, "-> R%d", VM->bytecode[i - 1]);
						++component;
						tmp[0] = '\0';
						break;
					}
				}
				if (assemblyCode[a] == '\n') {
					while (i % VM->sizeOfInstruction != 0) {
						VM->bytecode[i++] = 0;
					}
					instIndex = (i / VM->sizeOfInstruction);
					component = 0;
				}
			}
			else {
				if (assemblyCode[a] == ';') {
					component = 4;
				}
				else if (component == 0) {
					++component;
				}
				tmp[j] = assemblyCode[a];
				UPPERCASE(tmp[j]);
				++j;
			}
			++a;
		}
		DisposeTable(labels);
	}
	free(assemblyCode);

	//Ends the debug output
	printf("\n \n");

	//The final instruction should always be HALT
	while (i < VM->sizeOfBytecode) {
		VM->bytecode[i++] = '\0';
	} ++VM->nInstructions;

	//Generates Livecode based of the current bytecode
	GenLivecode();
}

//B4A::ignore
void GenLivecode(void)
{
	//Allocates memory for the livecode array	
	size_t livecodeSize = (sizeof(VMInst) * (size_t)VM->nInstructions);
	if (VM->livecode) free(VM->livecode);
	VM->livecode = (VMInst*)SafeMalloc(livecodeSize);

	//Populate the livecode array
	size_t p, c = 0; 
	int64_t inst; data64 data;
	byte currInst, lastInst = 0;
	for (inst = 0; inst < VM->nInstructions; ++inst) {
		for (p = 0; p < VM->sizeOfInt64; ++p) {
			((char*)(&data.i64))[p] = *(VM->bytecode + c + 1 + p);
		}
		VM->livecode[inst].inst = VM->bytecode[c];
		VM->livecode[inst].rgtr = VM->bytecode[c + 9];
		VM->livecode[inst].data = data;

		//Prefetch labels from jump instructions
		VM->livecode[inst].jump = -1;
		VM->livecode[inst].jins = 0;
		currInst = VM->livecode[inst].inst;
		if  (( lastInst == INDEX_CMP
		    || lastInst == INDEX_FCMP
		    || lastInst == INDEX_RCMP
		    || lastInst == INDEX_RFCMP
		    || lastInst == INDEX_RCMPV
		    || lastInst == INDEX_RFCMPV)
		    && 
		    (  currInst == INDEX_JNE
		    || currInst == INDEX_JEQ
		    || currInst == INDEX_JG
		    || currInst == INDEX_JL
		    || currInst == INDEX_JGE
		    || currInst == INDEX_JLE
		    ))
		{
			VM->livecode[inst - 1].jump = VM->livecode[inst].data.i64;
			VM->livecode[inst - 1].jins = (currInst - INDEX_JNE) + 1;
		}
		lastInst = VM->livecode[inst].inst;

		//Advances to the next instruction
		c += VM->sizeOfInstruction;
	}
}

//Logs the loaded bytecode in hex format
void ShowBytecode(void)
{
	int64_t i, size;
	size = VM->nInstructions * VM->sizeOfInstruction;
	if (size == 0) {
		printf("TinyVM - Bytecode: NULL\n");
		return;
	}
	printf("TinyVM - Bytecode:\n");
	printf("-----------------------------\n");
	char textOutput[80];
	textOutput[0] = '\0';
	for (i = 0; i <= size; ++i) {
		if (i > 0 && i % VM->sizeOfInstruction == 0) {
			printf("%s\n", textOutput);
			textOutput[0] = '\0';
			if (i == size) break;
		}
		ADD_STRING_ARGS_TO(textOutput, "%02X ", VM->bytecode[i]);
	}
	printf(" \n");
}

//Loads the given bytecode into the Virtual Machine
void Load(char* bytecode, size_t bytecodeLength)
{
	VM->nInstructions = bytecodeLength / VM->sizeOfInstruction;
	VM->bytecode = (byte*)SafeMalloc(bytecodeLength);
	size_t i;
	for (i = 0; i < VM->sizeOfBytecode; ++i) {
		VM->bytecode[i] = (byte)bytecode[i];
	}	
	GenLivecode();
}

//Executes the loaded bytecode
void Execute(void)
{
	PREPARE_VM; START_VM;
	register VMInst* liveCode = VM->livecode;
	while (VM->state == VMSTATE_RUNNING) {
		//VM->Inst[liveCode[VM->ip].inst]();
		EXECUTE_THE_CURRENT_INSTRUCTION;
		++VM->ip;
	}
	--VM->state;
}

//Executes the loaded bytecode in debug mode
void ExecuteInDebugMode()
{
	char textOutput[80];
	PREPARE_VM; START_VM;
	MakeSureThat(THERE_IS_CODE_TO_EXECUTE);
	ShowBytecode();	printf("TinyVM - Executing bytecode...\n");
	register VMInst* liveCode = VM->livecode;
	while (VM->state == VMSTATE_RUNNING) {
		//VM->Inst[liveCode[VM->ip].inst]();
		EXECUTE_THE_CURRENT_INSTRUCTION;
		//Debug output:
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		textOutput[0] = '\0'; ADD_STRING_ARGS_TO(textOutput, "% 8s  0x%08X 0x%08X", instructionTable[VM->livecode[VM->ip].inst].key, DATA_BUFFER.i32[0], DATA_BUFFER.i32[1]);
		if (instructionTable[VM->livecode[VM->ip].inst].nfo == 0x1) ADD_STRING_ARGS_TO(textOutput, " -> R%d: 0x%016llX", OUTPUT_REGISTER, VM->regs[OUTPUT_REGISTER].i64);
		printf("%s\n", textOutput);
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		++VM->ip;
	}
	--VM->state;
	//Debug output:
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	char VMState[16];
	VMState[0] = '\0';
	if (VM->state == 0x00 || VM->state == 0xFF) { ADD_STRING_TO(VMState, "STOPPED"); }
	else { ADD_STRING_TO(VMState, "PAUSED"); }
	printf("TinyVM - Execution terminated (%s).\n", VMState);
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}

//Returns the loaded bytecode in binary format
byte *Bytecode(void)
{
	//@NLG: Returns array (VM->sizeOfBytecode)
	return VM->bytecode;
}

//Returns the value of the given register as a 64-bit integer
int64_t RegisterInt64(short n)
{
	return VM->regs[n].i64;
}

//Returns the value of the given register as a 64-bit float
double RegisterFloat64(short n)
{
	return VM->regs[n].f64;
}

//Returns the value of the given register as an array of 8 unsigned bytes
byte *RegisterBytes(short n)
{
	//@NLG: Returns array (VM->sizeOfData64)
	return VM->regs[n].byt;
}

//Returns the values of all registers as an array of 10x8 unsigned bytes
byte *Registers(void)
{
	//@NLG: Returns array (VM->sizeOfData64 * 10)
	return (byte*)VM->regs[0].byt;
}

//Returns the stack usage in percentage
double StackUsage(void)
{
	return (double)VM->sp / (double)VM->stackLength;
}

uint64_t StackPointer(void)
{
	return VM->sp;
}

//Wipes the stack and resizes it accordingly
size_t SetStackLength(size_t length)
{
	MakeSureThat(STACK_LENGTH_IS_NOT_ZERO);
	free(VM->stack); VM->sp = -1; 
	size_t bytes = length * VM->sizeOfData64;
	VM->stack = (data64*)SafeMalloc(bytes);
	VM->stackLength = length;
	return bytes;
}

//Returns the entire stack as an array of bytes
byte *StackAsByteArray(void)
{
	//@NLG: Returns array (VM->sizeOfData64 * (VM->sp + 1))
	return (byte*)VM->stack;
}

//Returns the entire stack as an array of 64-bit integers
int64_t *StackAsLongArray(void)
{
	//@NLG: Returns array (VM->sp + 1)
	return (int64_t*)VM->stack;
}

//Returns the entire stack as an array of 64-bit floats
double *StackAsDoubleArray(void)
{
	//@NLG: Returns array (VM->sp + 1)
	return (double*)VM->stack;
}

//Returns the Virtual Machine's current state
byte State(void)
{
	return VM->state;
}

//Returns the Virtual Machine's native heap memory address
uintptr_t MemoryAddress(void)
{
	return VM->memoryAddress;
}

//B4A::ignore
void AddNode(dynamicTable *tbl, char *key, int64_t val)
{
	tableNode *node = (tableNode*)SafeMalloc(sizeof(tableNode));
	size_t i = 0;
	while (key && i < 16) {
		node->key[i] = key[i];
		++i;
	}
	node->val = val;
	node->next = NULL;
	if (tbl->rootNode == NULL) {
		tbl->rootNode = node;
		tbl->currNode = tbl->rootNode;
	}
	else {
		tbl->currNode->next = node;
		tbl->currNode = node;
	}
	++tbl->length;
}

//B4A::ignore
dynamicTable *NewTable(void)
{
	dynamicTable* tbl = (dynamicTable*)SafeMalloc(sizeof(dynamicTable));
	tbl->currNode = NULL;
	tbl->rootNode = NULL;
	tbl->length = 0;
	return tbl;
}

//B4A::ignore
void DisposeTable(dynamicTable *tbl)
{
	tableNode* node = tbl->rootNode;
	while (node)
	{
		tbl->currNode = node;
		node = node->next;
		free(tbl->currNode);
	}
	free(tbl);
}

//B4A::ignore
int64_t Int64FromHex(char* data)
{
	int64_t value;
	byte i, j, k;
	byte n = 7;
	for (i = 2; i < 17; i += 2)
	{
		j = i - 2;
		k = i - 1;
		byte a, b;
		if (data[j] >= '0' && data[j] <= '9')
		{
			a = data[j] - 48; // data[j] - '0'
		}
		else if (data[j] >= 'A' && data[j] <= 'F')
		{
			a = data[j] - 55; // data[j] - 'A' + 10
		}
		else if (data[j] >= 'a' && data[j] <= 'f')
		{
			a = data[j] - 87; // data[j] - 'a' + 10
		}
		else
		{
			a = 0;
		}
		if (data[k] >= '0' && data[k] <= '9')
		{
			b = data[k] - 48; // data[j] - '0'
		}
		else if (data[k] >= 'A' && data[k] <= 'F')
		{
			b = data[k] - 55; // data[j] - 'A' + 10
		}
		else if (data[k] >= 'a' && data[k] <= 'f')
		{
			b = data[k] - 87; // data[j] - 'a' + 10
		}
		else
		{
			b = 0;
		}
		((char*)(&value))[n] = (a * 16) + b;
		--n;
	}
	return value;
}

//B4A::ignore
double StrToNum(char *str)
{
	int64_t result = 0;
	int64_t power = 1;
	size_t i = strlen(str);
	while (str[--i] != '.')
	{
		if (str[i] == '-') {
			return (double)-result;
		}
		result += (str[i] ^ '0') * power;		
		if (!i) return (double)result;
		power *= 10;
	}
	byte minus = 0;
	long double number = result / (double)power;
	power = 1;
	result = 0;
	while (i--)
	{
		if (str[i] == '-') {
			minus = 1;
			break;
		}
		result += (str[i] ^ '0') * power;
		power *= 10;
	}
	number += result;
	if (minus) {
		return (double)(-number);
	}
	return (double)(number);
}

//B4A::ignore
void ShowBytes(const void *object, size_t size)
{
	const unsigned char* const bytes = (const unsigned char*)object;
	printf("[ ");
	size_t i;
	for (i = 0; i < size; i++) {
		printf("%02x ", bytes[i]);
	}
	printf("]\n");
}

//B4A::ignore
void MakeSureThat(byte condition, char* errorMsg, int exitCode)
{
	if (condition) return;
	printf("%s\n", errorMsg); HOLD;
	exit(exitCode);	
}

//B4A::ignore
void *SafeMalloc(size_t size)
{
	void *ptr = malloc(size);
	MakeSureThat(MALLOC_RETURNS_A_VALID_ADDRESS(ptr));
	return ptr;
}
//*******************************************************************
//* END OF NLG CODE                                                 *
//*******************************************************************