#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>


//Internal Debug Macros
#define SHOW_TOP                           if (VM->stack[VM->sp].i64 > 9999 || VM->stack[VM->sp].i64 < -9999) printf("[%02d]     INT:  ...     FLOAT: %7.3f     HEX: 0x%016llX\n", VM->sp, VM->stack[VM->sp].f64, VM->stack[VM->sp].i64); else printf("[%02d]     INT: %4I64d     FLOAT: %7.3f     HEX: 0x%016llX\n", VM->sp, VM->stack[VM->sp].i64, VM->stack[VM->sp].f64, VM->stack[VM->sp].i64)
#define SHOW_REG(r)                        printf("R%d: %I64d\n", r, VM->regs[r].i64)
#define HOLD                               //getch()

//MakeSureThat() Macros
#define MALLOC_RETURNS_A_VALID_ADDRESS(a)  a != NULL                                        , "MEMORY ALLOCATION ERROR: Null address returned."      , 10
#define LABEL_IS_NOT_AN_EMPTY_STRING       assemblyCode[i - 1] > ' '                        , "COMPILE ERROR: Label is empty."                       , 20
#define LABEL_DOESNT_START_WITH_A_DIGIT    assemblyCode[j] < '0' || assemblyCode[j] > '9'   , "COMPILE ERROR: Label starts with a digit."            , 21
#define LABEL_DOESNT_EXCEED_MAXLENGTH      k < 16                                           , "COMPILE ERROR: Label exceeds max length (16)."        , 22
#define THERE_ARE_NO_SPACES_BETWEEN_ARGS   tmp[3] != '\0'                                   , "COMPILE ERROR: Spaces between arguments not allowed." , 23
#define THERE_IS_CODE_TO_EXECUTE           VM->bytecode != NULL && VM->livecode != NULL     , "EXECUTION ERROR: No instructions found."              , 30
#define STACK_LENGTH_IS_NOT_ZERO           length > 0                                       , "STACK SIZE ERROR: Null size request."                 , 40

//VM State
#define VMSTATE_STOPPED                    0
#define VMSTATE_RUNNING                    1
#define VMSTATE_PAUSED                     2

//VM Control
#define RESET_VM                           VM->sp = -1; VM->ip = 0; size_t vmi; for (vmi = 0; vmi < 10; ++vmi) { VM->regs[vmi].i64 = 0; } for (vmi = 0; vmi < 256; ++vmi) { VM->stack[vmi].i64 = 0; }
#define PREPARE_VM                         if (VM->state != VMSTATE_RUNNING) { RESET_VM; }
#define START_VM                           VM->state = VMSTATE_RUNNING
#define PAUSE_VM                           VM->state = VMSTATE_PAUSED
#define STOP_VM                            VM->state = VMSTATE_STOPPED

//VM Data
#define DATA_BUFFER                        VM->livecode[VM->ip].data
#define VALUE                              DATA_BUFFER.i32[1]
#define FVALUE                             DATA_BUFFER.f32[1]

//VM Registers
#define REG_A                              VM->regs[DATA_BUFFER.i32[0]]
#define REG_B                              VM->regs[DATA_BUFFER.i32[1]]
#define REG_C                              VM->regs[VM->livecode[VM->ip].rgtr]
#define REG_X                              VM->regs[DATA_BUFFER.i64]
#define REG_ZERO                           VM->regs[0]
#define REGISTER(x)                        VM->regs[x]
#define OUTPUT_REGISTER                    VM->livecode[VM->ip].rgtr
#define CLEAR_REGISTERS                    VM->regs[0].i64 = 0; VM->regs[1].i64 = 0; VM->regs[2].i64 = 0; VM->regs[3].i64 = 0; VM->regs[4].i64 = 0; VM->regs[5].i64 = 0; VM->regs[6].i64 = 0; VM->regs[7].i64 = 0; VM->regs[8].i64 = 0; VM->regs[9].i64 = 0

//VM Temporary Variables
#define TMP_A                              VM->tmp[0]
#define TMP_B                              VM->tmp[1]
#define BASE                               VM->tmp[0]
#define EXP                                VM->tmp[1]
#define RESULT                             VM->tmp[0]
#define ITR                                VM->itr

//VM Stack Control
#define STACK_TOP_TO_REGISTER              REGISTER(OUTPUT_REGISTER).i64 = STACK_READ.i64
#define STACK_READ                         VM->stack[VM->sp]
#define STACK_WRITE_I64(object)            VM->stack[VM->sp].i64 = object; //STACK_TOP_TO_REGISTER
#define STACK_WRITE_F64(object)            VM->stack[VM->sp].f64 = object; //STACK_TOP_TO_REGISTER
#define STACK_WRITE_I32(a,b)               VM->stack[VM->sp][0].i32 = a; VM->stack[VM->sp][1].i32 = b; //STACK_TOP_TO_REGISTER
#define STACK_WRITE_F32(a,b)               VM->stack[VM->sp][0].f32 = a; VM->stack[VM->sp][1].f32 = b; //STACK_TOP_TO_REGISTER
#define STACK_WRITE_F32(a,b)               VM->stack[VM->sp][0].f32 = a; VM->stack[VM->sp][1].f32 = b; //STACK_TOP_TO_REGISTER
#define STACK_PUSH_I64(object)             ++VM->sp; VM->stack[VM->sp].i64 = object; //STACK_TOP_TO_REGISTER
#define STACK_PUSH_F64(object)             ++VM->sp; VM->stack[VM->sp].f64 = object; //STACK_TOP_TO_REGISTER
#define STACK_PUSH_I32(a,b)                ++VM->sp; VM->stack[VM->sp][0].i32 = a; VM->stack[VM->sp][1].i32 = b; //STACK_TOP_TO_REGISTER
#define STACK_PUSH_F32(a,b)                ++VM->sp; VM->stack[VM->sp][0].f32 = a; VM->stack[VM->sp][1].f32 = b; //STACK_TOP_TO_REGISTER
#define STACK_PUSH_F32(a,b)                ++VM->sp; VM->stack[VM->sp][0].f32 = a; VM->stack[VM->sp][1].f32 = b; //STACK_TOP_TO_REGISTER
#define STACK_POP                          --VM->sp; STACK_TOP_TO_REGISTER

//VM Jump Control
#define JUMP_DESTINATION                   VM->livecode[VM->ip].jump
#define JUMP_INSTRUCTION                   VM->livecode[VM->ip].jins
#define CMP_JEQ(a, b)                      if (a == b) { VM->ip = VM->livecode[VM->ip].jump - 1; }	else { ++VM->ip; }
#define CMP_JNE(a, b)                      if (a != b) { VM->ip = VM->livecode[VM->ip].jump - 1; }	else { ++VM->ip; }
#define CMP_JL(a, b)                       if (a <  b) { VM->ip = VM->livecode[VM->ip].jump - 1; }	else { ++VM->ip; }
#define CMP_JG(a, b)                       if (a >  b) { VM->ip = VM->livecode[VM->ip].jump - 1; }	else { ++VM->ip; }
#define CMP_JLE(a, b)                      if (a <= b) { VM->ip = VM->livecode[VM->ip].jump - 1; }	else { ++VM->ip; }
#define CMP_JGE(a, b)                      if (a >= b) { VM->ip = VM->livecode[VM->ip].jump - 1; }	else { ++VM->ip; }

//Compiler Macros
#define UPPERCASE(c)                       if (c >= 97 && c <= 122) c -= 32
#define ADD_STRING_TO(ptr, text)           snprintf(ptr + strlen(ptr), sizeof(ptr), text)
#define ADD_STRING_ARGS_TO(ptr, text, ...) snprintf(ptr + strlen(ptr), sizeof(ptr), text, __VA_ARGS__)

//User Debug Macros
#define SHOW_CURRENT_INSTRUCTION           printf("% 8s  0x%08X 0x%08X -> R%d: 0x%016llX\n", instructionTable[VM->livecode[VM->ip].inst].key, DATA_BUFFER.i32[0], DATA_BUFFER.i32[1], OUTPUT_REGISTER, VM->regs[OUTPUT_REGISTER].i64);

//MSDN Max Macro
#define max(a, b)                          (((a) > (b)) ? (a) : (b))

//VM Compare and Jump Indexes
#define INDEX_CMP_ONLY                      0
#define INDEX_CMP_JNE                       1
#define INDEX_CMP_JEQ                       2
#define INDEX_CMP_JG                        3
#define INDEX_CMP_JL                        4
#define INDEX_CMP_JGE                       5
#define INDEX_CMP_JLE                       6

//VM Instruction Indexes
#define INDEX_HALT                          0
#define INDEX_PAUSE                         1
#define INDEX_PUSH                          2
#define INDEX_POP                           3
#define INDEX_CLR                           4
#define INDEX_SET                           5
#define INDEX_MOV                           6
#define INDEX_SWAP                          7
#define INDEX_ADD                           8
#define INDEX_SUB                           9
#define INDEX_MUL                          10
#define INDEX_DIV                          11
#define INDEX_MOD                          12
#define INDEX_POWER                        13
#define INDEX_SQRT                         14
#define INDEX_CHS                          15
#define INDEX_TOFLOAT                      16
#define INDEX_FADD                         17
#define INDEX_FSUB                         18
#define INDEX_FMUL                         19
#define INDEX_FDIV                         20
#define INDEX_FPOWER                       21
#define INDEX_FSQRT                        22
#define INDEX_FCHS                         23
#define INDEX_FCMP                         24
#define INDEX_TOINT                        25
#define INDEX_AND                          26
#define INDEX_OR                           27
#define INDEX_NEG                          28
#define INDEX_RSHIFT                       29
#define INDEX_LSHIFT                       30
#define INDEX_XOR                          31
#define INDEX_CMP                          32
#define INDEX_JMP                          33
#define INDEX_JNE                          34
#define INDEX_JEQ                          35
#define INDEX_JG                           36
#define INDEX_JL                           37
#define INDEX_JGE                          38
#define INDEX_JLE                          39
#define INDEX_RADD                         40
#define INDEX_RSUB                         41
#define INDEX_RMUL                         42
#define INDEX_RDIV                         43
#define INDEX_RMOD                         44
#define INDEX_RPOWER                       45
#define INDEX_RSQRT                        46
#define INDEX_RCHS                         47
#define INDEX_RCMP                         48
#define INDEX_RINC                         49
#define INDEX_RDEC                         50
#define INDEX_RTOINT                       51
#define INDEX_RTOFLOAT                     52
#define INDEX_RCMPV                        53
#define INDEX_RADDV                        54
#define INDEX_RSUBV                        55
#define INDEX_RMULV                        56
#define INDEX_RDIVV                        57
#define INDEX_RMODV                        58
#define INDEX_RPOWERV                      59
#define INDEX_RFADD                        60
#define INDEX_RFSUB                        61
#define INDEX_RFMUL                        62
#define INDEX_RFDIV                        63
#define INDEX_RFPOWER                      64
#define INDEX_RFSQRT                       65
#define INDEX_RFCHS                        66
#define INDEX_RFCMP                        67
#define INDEX_RFCMPV                       68
#define INDEX_RFADDV                       69
#define INDEX_RFSUBV                       70
#define INDEX_RFMULV                       71
#define INDEX_RFDIVV                       72
#define INDEX_RFPOWERV                     73
#define INDEX_RAND                         74
#define INDEX_ROR                          75
#define INDEX_RNEG                         76
#define INDEX_RRSHIFT                      77
#define INDEX_RLSHIFT                      78
#define INDEX_RXOR                         79
#define INDEX_RANDV                        80
#define INDEX_RORV                         81
#define INDEX_RXORV                        82
#define INDEX_RSP                          83
#define INDEX_PUSHINT                      84
#define INDEX_PUSHFLOAT                    85

typedef unsigned char                      byte;
typedef struct TinyVM                      TinyVM;
typedef struct table                       table;
typedef struct tableNode                   tableNode;
typedef struct dynamicTable                dynamicTable;
typedef struct VMInst                      VMInst;
typedef union  data64                      data64;

union data64 {
	int64_t i64;
	double  f64;
	int32_t i32[2];
	float   f32[2];
	byte    byt[8];
	byte    str[8];
};

struct table {
	char *key;
	byte  val;
	byte  nfo;
};

struct tableNode {
	tableNode *next;
	char      key[16];
	int64_t   val;
};

struct dynamicTable {
	tableNode *rootNode;
	tableNode *currNode;
	size_t length;
};

struct VMInst {
	int64_t jump;
	data64  data;
	byte    inst;
	byte    rgtr;
	byte    jins;
};

struct TinyVM
{
	uintptr_t memoryAddress;
	size_t    stackLength;
	size_t    sizeOfInt64;
	size_t    sizeOfByte;
	size_t    sizeOfChar;
	size_t    sizeOfData64;
	size_t    sizeOfBytecode;
	size_t    sizeOfInstruction;
	int64_t   nInstructions;
	int64_t   itr;
	uint64_t  ip;
	uint64_t  sp;
	data64    tmp[2];
	data64    regs[10];
	data64    *stack;
	byte      state;
	byte      littleEndian;
	byte      *bytecode;
	VMInst    *livecode;
};

//Global Objects
static TinyVM  *VM = NULL;

//B4A Exposed Functions
void           Initialize          (void);
void           Compile             (char*);
void           Load                (char*, size_t);
void           Execute             (void);
void           ExecuteInDebugMode  (void);
void           ShowBytecode        (void);
byte           *Bytecode           (void);
void           Kill                (void);
int64_t        RegisterInt64       (short);
double         RegisterFloat64     (short);
byte           *RegisterBytes      (short);
byte           *Registers          (void);
byte           State               (void);
size_t         SetStackLength      (size_t);
byte           *StackAsByteArray   (void);
int64_t        *StackAsLongArray   (void);
double         *StackAsDoubleArray (void);
uintptr_t      MemoryAddress       (void);
double         StackUsage          (void);
uint64_t       StackPointer        (void);

//Private Functions
void           GenLivecode         (void);
dynamicTable   *NewTable           (void);
void           AddNode             (dynamicTable*, char*, int64_t);
void           DisposeTable        (dynamicTable*);
double         StrToNum            (char*);
int64_t        Int64FromHex        (char*);
void           ShowBytes           (const void*, size_t);
void           MakeSureThat        (byte, char*, int);
void           *SafeMalloc         (size_t);

//Instruction Table
static table instructionTable[] = {
	{  "HALT",      INDEX_HALT,      0  },
	{  "PAUSE",     INDEX_PAUSE,     0  },
	{  "CLR",       INDEX_CLR,       0  },
	{  "POP",       INDEX_POP,       1  },
	{  "PUSH",      INDEX_PUSH,      1  },
	{  "SET",       INDEX_SET,       1  },
	{  "MOV",       INDEX_MOV,       1  },
	{  "SWAP",      INDEX_SWAP,      0  },
	{  "ADD",       INDEX_ADD,       1  },
	{  "SUB",       INDEX_SUB,       1  },
	{  "MUL",       INDEX_MUL,       1  },
	{  "DIV",       INDEX_DIV,       1  },
	{  "MOD",       INDEX_MOD,       1  },
	{  "POWER",     INDEX_POWER,     1  },
	{  "SQRT",      INDEX_SQRT,      1  },
	{  "CHS",       INDEX_CHS,       1  },
	{  "TOFLOAT",   INDEX_TOFLOAT,   1  },
	{  "FADD",      INDEX_FADD,      1  },
	{  "FSUB",      INDEX_FSUB,      1  },
	{  "FMUL",      INDEX_FMUL,      1  },
	{  "FDIV",      INDEX_FDIV,      1  },
	{  "FPOWER",    INDEX_FPOWER,    1  },
	{  "FSQRT",     INDEX_FSQRT,     1  },
	{  "FCHS",      INDEX_FCHS,      1  },
	{  "FCMP",      INDEX_FCMP,      1  },
	{  "TOINT",     INDEX_TOINT,     1  },
	{  "AND",       INDEX_AND,       1  },
	{  "OR",        INDEX_OR,        1  },
	{  "NEG",       INDEX_NEG,       1  },
	{  "RSHIFT",    INDEX_RSHIFT,    1  },
	{  "LSHIFT",    INDEX_LSHIFT,    1  },
	{  "XOR",       INDEX_XOR,       1  },
	{  "CMP",       INDEX_CMP,       1  },
	{  "JMP",       INDEX_JMP,       0  },
	{  "JNE",       INDEX_JNE,       0  },
	{  "JEQ",       INDEX_JEQ,       0  },
	{  "JG",        INDEX_JG,        0  },
	{  "JL",        INDEX_JL,        0  },
	{  "JGE",       INDEX_JGE,       0  },
	{  "JLE",       INDEX_JLE,       0  },
	{  "RADD",      INDEX_RADD,      1  },
	{  "RSUB",      INDEX_RSUB,      1  },
	{  "RMUL",      INDEX_RMUL,      1  },
	{  "RDIV",      INDEX_RDIV,      1  },
	{  "RMOD",      INDEX_RMOD,      1  },
	{  "RPOWER",    INDEX_RPOWER,    1  },
	{  "RSQRT",     INDEX_RSQRT,     1  },
	{  "RCHS",      INDEX_RCHS,      1  },
	{  "RCMP",      INDEX_RCMP,      1  },
	{  "RINC",      INDEX_RINC,      1  },
	{  "RDEC",      INDEX_RDEC,      1  },
	{  "RTOINT",    INDEX_RTOINT,    1  },
	{  "RTOFLOAT",  INDEX_RTOFLOAT,  1  },
	{  "RCMPV",     INDEX_RCMPV,     1  },
	{  "RADDV",     INDEX_RADDV,     1  },
	{  "RSUBV",     INDEX_RSUBV,     1  },
	{  "RMULV",     INDEX_RMULV,     1  },
	{  "RDIVV",     INDEX_RDIVV,     1  },
	{  "RMODV",     INDEX_RMODV,     1  },
	{  "RPOWERV",   INDEX_RPOWERV,   1  },
	{  "RFADD",     INDEX_RFADD,     1  },
	{  "RFSUB",     INDEX_RFSUB,     1  },
	{  "RFMUL",     INDEX_RFMUL,     1  },
	{  "RFDIV",     INDEX_RFDIV,     1  },
	{  "RFPOWER",   INDEX_RFPOWER,   1  },
	{  "RFSQRT",    INDEX_RFSQRT,    1  },
	{  "RFCHS",     INDEX_RCHS,      1  },
	{  "RFCMP",     INDEX_RFCMP,     1  },
	{  "RFCMPV",    INDEX_RFCMPV,    1  },
	{  "RFADDV",    INDEX_RFADDV,    1  },
	{  "RFSUBV",    INDEX_RFSUBV,    1  },
	{  "RFMULV",    INDEX_RFMULV,    1  },
	{  "RFDIVV",    INDEX_RFDIVV,    1  },
	{  "RFPOWERV",  INDEX_RFPOWERV,  1  },
	{  "RAND",      INDEX_RAND,      1  },
	{  "ROR",       INDEX_ROR,       1  },
	{  "RNEG",      INDEX_RNEG,      1  },
	{  "RRSHIFT",   INDEX_RRSHIFT,   1  },
	{  "RLSHIFT",   INDEX_RLSHIFT,   1  },
	{  "RXOR",      INDEX_RXOR,      1  },
	{  "RANDV",     INDEX_RANDV,     1  },
	{  "RORV",      INDEX_RORV,      1  },
	{  "RXORV",     INDEX_RXORV,     1  },
	{  "RSP",       INDEX_RSP,       0  },
	{  "PUSHINT",   INDEX_PUSHINT,   1  },
	{  "PUSHFLOAT", INDEX_PUSHFLOAT, 1  }
};
#define NUMBER_OF_INSTRUCTIONS \
(sizeof(instructionTable) / sizeof(table))

//Switch/Case Instruction Macro
#define EXECUTE_THE_CURRENT_INSTRUCTION \
		switch (liveCode[VM->ip].inst) \
		{ \
			case INDEX_HALT: \
				STOP_VM; \
				break; \
 \
 \
			case INDEX_PAUSE: \
				PAUSE_VM; \
				break; \
 \
 \
			case INDEX_PUSH: \
				STACK_PUSH_I64(REG_X.i64); \
				break; \
 \
 \
			case INDEX_PUSHINT: \
				STACK_PUSH_I64(DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_PUSHFLOAT: \
				STACK_PUSH_F64(DATA_BUFFER.f64); \
				break; \
 \
 \
 \
			case INDEX_ADD: \
				STACK_WRITE_I64(STACK_READ.i64 + DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_SUB: \
				STACK_WRITE_I64(STACK_READ.i64 - DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_MUL: \
				STACK_WRITE_I64(STACK_READ.i64 * DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_DIV: \
				STACK_WRITE_I64(STACK_READ.i64 / DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_MOD: \
				STACK_WRITE_I64(STACK_READ.i64 % DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_POWER: \
				if (DATA_BUFFER.i64 > 1) { \
					RESULT.i64 = STACK_READ.i64; \
					for (ITR = 0; ITR < DATA_BUFFER.i64; ++ITR) { \
						RESULT.i64 *= STACK_READ.i64; \
					} \
					STACK_WRITE_I64(RESULT.i64); \
				} \
				else if (DATA_BUFFER.i64 == 0) { \
					STACK_WRITE_I64(1); \
				} \
				else { \
					STACK_WRITE_I64(0); \
				} \
				break; \
 \
 \
			case INDEX_SQRT: \
				STACK_PUSH_I64((int64_t)sqrt((double)DATA_BUFFER.i64)); \
				break; \
 \
 \
			case INDEX_CHS: \
				STACK_WRITE_I64(-STACK_READ.i64); \
				break; \
 \
 \
			case INDEX_AND: \
				STACK_WRITE_I64(STACK_READ.i64 & DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_OR: \
				STACK_WRITE_I64(STACK_READ.i64 | DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_NEG: \
				STACK_WRITE_I64(~STACK_READ.i64); \
				break; \
 \
 \
			case INDEX_RSHIFT: \
				STACK_WRITE_I64(STACK_READ.i64 >> DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_LSHIFT: \
				STACK_WRITE_I64(STACK_READ.i64 << DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_XOR: \
				STACK_WRITE_I64(STACK_READ.i64 ^ DATA_BUFFER.i64); \
				break; \
 \
 \
			case INDEX_CMP: \
				REG_ZERO.i64 = (STACK_READ.i64 < DATA_BUFFER.i64 ? -1 : (STACK_READ.i64 > DATA_BUFFER.i64)); \
				break; \
 \
 \
			case INDEX_TOFLOAT: \
				STACK_WRITE_F64((double)STACK_READ.i64); \
				break; \
 \
 \
			case INDEX_FADD: \
				STACK_WRITE_F64(STACK_READ.f64 + DATA_BUFFER.f64;); \
				break; \
 \
 \
			case INDEX_FSUB: \
				STACK_WRITE_F64(STACK_READ.f64 - DATA_BUFFER.f64;); \
				break; \
 \
 \
			case INDEX_FMUL: \
				STACK_WRITE_F64(STACK_READ.f64 * DATA_BUFFER.f64;); \
				break; \
 \
 \
			case INDEX_FDIV: \
				STACK_WRITE_F64(STACK_READ.f64 / DATA_BUFFER.f64;); \
				break; \
 \
 \
			case INDEX_FPOWER: \
				if (DATA_BUFFER.f64 == 0.0) { \
					STACK_WRITE_F64(1.0); \
				} \
				else { \
					STACK_WRITE_F64(pow(STACK_READ.f64, DATA_BUFFER.f64)); \
				} \
				break; \
 \
 \
			case INDEX_FSQRT: \
				STACK_PUSH_F64(sqrt(DATA_BUFFER.f64)); \
				break; \
 \
 \
			case INDEX_FCHS: \
				STACK_WRITE_F64(-STACK_READ.f64); \
				break; \
 \
 \
 \
			case INDEX_FCMP: \
				REG_ZERO.i64 = (STACK_READ.f64 < DATA_BUFFER.f64 ? -1 : (STACK_READ.f64 > DATA_BUFFER.f64)); \
				break; \
 \
 \
			case INDEX_TOINT: \
				STACK_WRITE_I64((int64_t)STACK_READ.f64); \
				break; \
 \
 \
			case INDEX_POP: \
				STACK_POP; \
				break; \
 \
 \
			case INDEX_JMP: \
				VM->ip = DATA_BUFFER.i64 - 1; \
				break; \
 \
 \
			case INDEX_JNE: \
				if (REG_ZERO.i64) { \
					VM->ip = DATA_BUFFER.i64 - 1; \
				} \
				break; \
 \
 \
			case INDEX_JEQ: \
				if (!REG_ZERO.i64) { \
					VM->ip = DATA_BUFFER.i64 - 1; \
				} \
				break; \
 \
 \
			case INDEX_JG: \
				if (REG_ZERO.i64 == 1) { \
					VM->ip = DATA_BUFFER.i64 - 1; \
				} \
				break; \
 \
 \
			case INDEX_JL: \
				if (REG_ZERO.i64 == -1) { \
					VM->ip = DATA_BUFFER.i64 - 1; \
				} \
				break; \
 \
 \
			case INDEX_JGE: \
				if (REG_ZERO.i64 != -1) { \
					VM->ip = DATA_BUFFER.i64 - 1; \
				} \
				break; \
 \
 \
			case INDEX_JLE: \
				if (REG_ZERO.i64 != 1) { \
					VM->ip = DATA_BUFFER.i64 - 1; \
				} \
				break; \
 \
 \
			case INDEX_RADD: \
				REGISTER(OUTPUT_REGISTER).i64 = (REG_A.i64 + REG_B.i64); \
				break; \
 \
 \
			case INDEX_RSUB: \
				REGISTER(OUTPUT_REGISTER).i64 = (REG_A.i64 - REG_B.i64); \
				break; \
 \
 \
			case INDEX_RMUL: \
				REGISTER(OUTPUT_REGISTER).i64 = (REG_A.i64 * REG_B.i64); \
				break; \
 \
 \
			case INDEX_RDIV: \
				REGISTER(OUTPUT_REGISTER).i64 = (REG_A.i64 / REG_B.i64); \
				break; \
 \
 \
			case INDEX_RMOD: \
				REGISTER(OUTPUT_REGISTER).i64 = (REG_A.i64 % REG_B.i64); \
				break; \
 \
 \
			case INDEX_RPOWER: \
				if (REG_B.i64 > 1) { \
					TMP_A.i64 = REG_A.i64; \
					for (ITR = 1; ITR < REG_B.i64; ++ITR) { \
						TMP_A.i64 *= REG_A.i64; \
					} \
					REGISTER(OUTPUT_REGISTER).i64 = TMP_A.i64; \
				} \
				else if (REG_B.i64 == 1) { \
					REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64; \
				} \
				else if (REG_B.i64 == 0) { \
					REGISTER(OUTPUT_REGISTER).i64 = 1; \
				} \
				else { \
					REGISTER(OUTPUT_REGISTER).i64 = 0; \
				} \
				break; \
 \
 \
			case INDEX_RSQRT: \
				REGISTER(OUTPUT_REGISTER).i64 = (int64_t)sqrt((double)REG_X.i64); \
				break; \
 \
 \
			case INDEX_RCHS: \
				REGISTER(OUTPUT_REGISTER).i64 = -REG_X.i64; \
				break; \
 \
 \
			case INDEX_RCMP: \
				switch (JUMP_INSTRUCTION) \
				{ \
					case INDEX_CMP_ONLY: \
						REGISTER(OUTPUT_REGISTER).i64 = (REG_A.i64 < REG_B.i64 ? -1 : (REG_A.i64 > REG_B.i64)); \
						break; \
				 \
				 \
					case INDEX_CMP_JNE: \
						CMP_JNE(REG_A.i64, REG_B.i64) \
						break; \
				 \
				 \
					case INDEX_CMP_JEQ: \
						CMP_JEQ(REG_A.i64, REG_B.i64) \
						break; \
				 \
				 \
					case INDEX_CMP_JG: \
						CMP_JG(REG_A.i64, REG_B.i64) \
						break; \
				 \
				 \
					case INDEX_CMP_JL: \
						CMP_JL(REG_A.i64, REG_B.i64) \
						break; \
				 \
				 \
					case INDEX_CMP_JGE: \
						CMP_JGE(REG_A.i64, REG_B.i64) \
						break; \
				 \
				 \
					case INDEX_CMP_JLE: \
						CMP_JLE(REG_A.i64, REG_B.i64) \
						break; \
				} \
				break; \
 \
 \
			case INDEX_RFADD: \
				REGISTER(OUTPUT_REGISTER).f64 = (REG_A.f64 + REG_B.f64); \
				break; \
 \
 \
			case INDEX_RFSUB: \
				REGISTER(OUTPUT_REGISTER).f64 = (REG_A.f64 - REG_B.f64); \
				break; \
 \
 \
			case INDEX_RFMUL: \
				REGISTER(OUTPUT_REGISTER).f64 = (REG_A.f64 * REG_B.f64); \
				break; \
 \
 \
			case INDEX_RFDIV: \
				REGISTER(OUTPUT_REGISTER).f64 = (REG_A.f64 / REG_B.f64); \
				break; \
 \
 \
			case INDEX_RFPOWER: \
				if (REG_B.f64 == 0.0) { \
					REGISTER(OUTPUT_REGISTER).f64 = 1.0; \
				} \
				else { \
					REGISTER(OUTPUT_REGISTER).f64 = pow(REG_A.f64, REG_B.f64); \
				} \
				break; \
 \
 \
			case INDEX_RFSQRT: \
				REGISTER(OUTPUT_REGISTER).f64 = sqrt(REG_X.f64); \
				break; \
 \
 \
			case INDEX_RFCHS: \
				REGISTER(OUTPUT_REGISTER).f64 = -REG_X.f64; \
				break; \
 \
 \
			case INDEX_RFCMP: \
				switch (JUMP_INSTRUCTION) \
				{ \
					case INDEX_CMP_ONLY: \
						REGISTER(OUTPUT_REGISTER).i64 = (REG_A.f64 < REG_B.f64 ? -1 : (REG_A.f64 > REG_B.f64)); \
						break; \
				 \
				 \
					case INDEX_CMP_JNE: \
						CMP_JNE(REG_A.f64, REG_B.f64) \
						break; \
				 \
				 \
					case INDEX_CMP_JEQ: \
						CMP_JEQ(REG_A.f64, REG_B.f64) \
						break; \
				 \
				 \
					case INDEX_CMP_JG: \
						CMP_JG(REG_A.f64, REG_B.f64) \
						break; \
				 \
				 \
					case INDEX_CMP_JL: \
						CMP_JL(REG_A.f64, REG_B.f64) \
						break; \
				 \
				 \
					case INDEX_CMP_JGE: \
						CMP_JGE(REG_A.f64, REG_B.f64) \
						break; \
				 \
				 \
					case INDEX_CMP_JLE: \
						CMP_JLE(REG_A.f64, REG_B.f64) \
						break; \
				} \
				break; \
 \
 \
			case INDEX_SET: \
				REGISTER(OUTPUT_REGISTER).i64 = DATA_BUFFER.i64; \
				break; \
 \
 \
			case INDEX_MOV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_X.i64; \
				break; \
 \
 \
			case INDEX_SWAP: \
				REG_A.i64 = REG_A.i64 + REG_B.i64; \
				REG_B.i64 = REG_A.i64 - REG_B.i64; \
				REG_A.i64 = REG_A.i64 - REG_B.i64; \
				break; \
 \
 \
			case INDEX_RCMPV: \
				switch (JUMP_INSTRUCTION) \
				{ \
					case INDEX_CMP_ONLY: \
						REGISTER(OUTPUT_REGISTER).i64 = (REG_A.i64 < VALUE ? -1 : (REG_A.i64 > VALUE)); \
						break; \
				 \
				 \
					case INDEX_CMP_JNE: \
						CMP_JNE(REG_A.i64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JEQ: \
						CMP_JEQ(REG_A.i64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JG: \
						CMP_JG(REG_A.i64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JL: \
						CMP_JL(REG_A.i64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JGE: \
						CMP_JGE(REG_A.i64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JLE: \
						CMP_JLE(REG_A.i64, VALUE) \
						break; \
				} \
				break; \
 \
 \
			case INDEX_RADDV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 + VALUE; \
				break; \
 \
 \
			case INDEX_RSUBV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 - VALUE; \
				break; \
 \
 \
			case INDEX_RMULV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 * VALUE; \
				break; \
 \
 \
			case INDEX_RDIVV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 / VALUE; \
				break; \
 \
 \
			case INDEX_RMODV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 % VALUE; \
				break; \
 \
 \
			case INDEX_RPOWERV: \
				if (VALUE > 1) { \
					TMP_A.i64 = REG_A.i64; \
					for (ITR = 1; ITR < VALUE; ++ITR) { \
						TMP_A.i64 *= REG_A.i64; \
					} \
					REGISTER(OUTPUT_REGISTER).i64 = TMP_A.i64; \
				} \
				else if (VALUE == 1) { \
					REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64; \
				} \
				else if (VALUE == 0) { \
					REGISTER(OUTPUT_REGISTER).i64 = 1; \
				} \
				else { \
					REGISTER(OUTPUT_REGISTER).i64 = 0; \
				} \
				break; \
 \
 \
			case INDEX_RINC: \
				REG_A.i64 += VALUE; \
				break; \
 \
 \
			case INDEX_RDEC: \
				REG_A.i64 -= VALUE; \
				break; \
 \
 \
			case INDEX_RTOINT: \
				REGISTER(OUTPUT_REGISTER).i64 = (int64_t)REG_X.f64; \
				break; \
 \
 \
			case INDEX_RTOFLOAT: \
				REGISTER(OUTPUT_REGISTER).f64 = (double)REG_X.i64; \
				break; \
 \
 \
			case INDEX_RFADDV: \
				REGISTER(OUTPUT_REGISTER).f64 = (REG_A.f64 + FVALUE); \
				break; \
 \
 \
			case INDEX_RFSUBV: \
				REGISTER(OUTPUT_REGISTER).f64 = (REG_A.f64 - FVALUE); \
				break; \
 \
 \
			case INDEX_RFMULV: \
				REGISTER(OUTPUT_REGISTER).f64 = (REG_A.f64 * FVALUE); \
				break; \
 \
 \
			case INDEX_RFDIVV: \
				REGISTER(OUTPUT_REGISTER).f64 = (REG_A.f64 / FVALUE); \
				break; \
 \
 \
			case INDEX_RFPOWERV: \
				if (FVALUE == 0.0) { \
					REGISTER(OUTPUT_REGISTER).f64 = 1.0; \
				} \
				else { \
					REGISTER(OUTPUT_REGISTER).f64 = pow(REG_A.f64, FVALUE); \
				} \
				break; \
 \
 \
			case INDEX_RFCMPV: \
				switch (JUMP_INSTRUCTION) \
				{ \
					case INDEX_CMP_ONLY: \
						REGISTER(OUTPUT_REGISTER).i64 = (REG_A.f64 < VALUE ? -1 : (REG_A.f64 > VALUE)); \
						break; \
				 \
				 \
					case INDEX_CMP_JNE: \
						CMP_JNE(REG_A.f64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JEQ: \
						CMP_JEQ(REG_A.f64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JG: \
						CMP_JG(REG_A.f64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JL: \
						CMP_JL(REG_A.f64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JGE: \
						CMP_JGE(REG_A.f64, VALUE) \
						break; \
				 \
				 \
					case INDEX_CMP_JLE: \
						CMP_JLE(REG_A.f64, VALUE) \
						break; \
				} \
				break; \
 \
 \
			case INDEX_RAND: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 & REG_B.i64; \
				break; \
 \
 \
			case INDEX_ROR: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 | REG_B.i64; \
				break; \
 \
 \
			case INDEX_RNEG: \
				REGISTER(OUTPUT_REGISTER).i64 = ~REG_X.i64; \
				break; \
 \
 \
			case INDEX_RRSHIFT: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 >> VALUE; \
				break; \
 \
 \
			case INDEX_RLSHIFT: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 << VALUE; \
				break; \
 \
 \
			case INDEX_RXOR: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 ^ REG_B.i64; \
				break; \
 \
 \
			case INDEX_RANDV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 & VALUE; \
				break; \
 \
 \
			case INDEX_RORV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 | VALUE; \
				break; \
 \
 \
			case INDEX_RXORV: \
				REGISTER(OUTPUT_REGISTER).i64 = REG_A.i64 ^ VALUE; \
				break; \
 \
 \
			case INDEX_RSP: \
				VM->sp = 0; \
				break; \
 \
 \
			case INDEX_CLR: \
				CLEAR_REGISTERS; \
				break; \
		}